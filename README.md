**Algorithms 2**
============
The implementation of abstract data types.

##### The project is divided into three parts: #####
 * c016.c: Hash-table
 * c401.c: Recursive implementation of operations on binary search tree
 * c402.c: Nonrecursive implementation of operations on binary search tree